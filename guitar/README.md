
# Guitar positions

### Data format

Vectors are given in the RelativeStringFret encoding:

```
RootFret (decimal number), relative positions (6 strings x (1+5) bits), count
00,000000000000010000000100000010000100,9102
...
03,000000010000000100000100000100000000,4458
```

The first data line describe the D major at the bottom of the neck.
The last data line describe the incomplete C major with a RootFret of 3.
See the Figure 9 and discusssion in (Cournut 2021).

### Changelog
 - july 2021: 1000 most frequent position vectors for chords (with 2-3 notes, with 4-6 notes)
 - upcoming: vectors split by genres

### Reference
J. Cournut et al.,
[https://hal.archives-ouvertes.fr/hal-03279863/](What are the most used guitar positions?), DLfM 2021
