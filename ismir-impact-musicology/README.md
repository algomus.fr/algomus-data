
# The Games We Play: Exploring The Impact of ISMIR on Musicology

This study focuses on an analysis of ten years’ worth of papers from 
the [International Society for Music Information Retrieval](https://www.ismir.net/conferences/) (ISMIR) from 2012 to 2021. 
Over 1000 citations of ISMIR papers were reviewed, and out of these, 
51 later works published in musicological venues drew from the findings of 28 ISMIR papers.

## Available data

- List of 699+ citations to 114 ISMIR papers, which claim to have some musicological utility (`The-Games-We-Play.xlsx`)

- Further analysis of the 143 citations in some musicological venue to 28 ISMIR papers, including the 51 citations which "somewhat used" the ISMIR contribution
(`The-Games-We-Play.xlsx` and `The-Games-We-Play.csv`)

## License

These data are made available under the [Open Database License](http://opendatacommons.org/licenses/odbl/1.0/).
Any rights in individual contents of the database are licensed under the [Database Contents License](http://opendatacommons.org/licenses/dbcl/1.0/).

## Reference

Vanessa Nina Borsan, Mathieu Giraud, Richard Groult. [The Games We Play: Exploring The Impact of ISMIR on Musicology](https://hal.science/hal-04142194),International Society for Music Information Retrieval Conference (ISMIR 2023), 2023,  545-552
