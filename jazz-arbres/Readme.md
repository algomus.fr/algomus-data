# Analyse et essai de reconstruction de structures arborescentes des grilles de jazz

This repository contains all tools detailed in `Patrice Thibaud, Mathieu Giraud. Analyse et essai de reconstruction de structures arborescentes des grilles de jazz. Journées d'Informatique Musicale (JIM 2023), May 2023, Paris, France. pp.62-67.`

https://hal.science/hal-04196644

# Files

The `src` folder contains all scripts developped and used to analyse JHT trees and rebuild them with our own algorithm.

* [analyse_tools.py](./src/analyse_tools.py) : a simple module to find intervals between two consecutives chords and generate stats on files
* [binary_tree.py](./src/binary_tree.py) : tool developped by computer science department of Universite de Lille in order to display binary trees with graphviz
* [evaluation_main.py](./src/evaluation_main.py) : a simple module to find intervals between two consecutives chords and generate stats on files
* [jazz_parser.py](./src/jazz_parser.py) : the parser for jazz files (of irB corpus) and some tools used for renaming chords according to JHT choices.
* [jht_list.py](./src/jht_list.py) : list of the 150(149) song's name of the JHT for iRb filename and JHT song's name
* [stats_jht.py](./src/stats_jht.py) : make statistics about brothers nodes, sisters leafs , depth from jht 
* [treebank_tools.py](./src/treebank_tools.py) : tools for building trees from JHT json file and find sisters, brothers nodes etc.
* [tree_compare_tools.py](./src/tree_compare_tools.py) : add counter labels to nodes (used in the process of evaluation of the rebuilt trees)
* [tree_rebuilt.py](./src/tree_rebuilt.py) : rebuilding trees with rules defined by our algorithm

# Dependencies

**python** _3.10_  
**music21** _5.7.2_  
**graphviz** _0.20.1_  

# HOW TO

## ANALYSE DE LA STRUCTURE ARBORESCENTE DES GRILLES

```bash
$ python3 ./stats_jht.py <param>
```

`<param>` can be : `brothers`, `sisters`, `leafs` or `depth`

_output directory_ : `../output_files/stats/`

## VERS UNE RECONSTRUCTION ALGORITHMIQUE DES ARBRES

```bash
$ python3 ./tree_rebuilt.py <songname>
```

`<songname>` : exact name as written in field `title` of the [JHT json file](treebank/treebank.json)
(default song : `Interplay`)

_output directory_ : `../output_files/trees/`  

execution steps displayed in the terminal and tree showed

## EVALUATION

```bash
$ python3 ./evaluation_main.py
```

_output directory_: `../output_files/evaluation/`

1 file / song  + `REBUILD_total_evaluation.txt` that summarised evaluations for all the corpus by sequences

# Data / References

The JHT dataset that this research builds on was presented in

`D. Harasim, C. Finkensiep, P. Ericson, T. J. O'Donnell, and M. Rohrmeier (2020). The Jazz Harmony Treebank. In Proceedings of the 21th International Society for Music Information Retrieval Conference, pp. 207-215. Montréal, Canada.` https://doi.org/10.5281/zenodo.4245406

The JSON file used can be find in [./treebank/treebank.json](./treebank/treebank.json)
