#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
from music21.harmony import ChordSymbol
from music21.interval import *
from music21.pitch import Pitch
from jazz_parser import replace_chord_symbol
from binary_tree import *

EMPTY = BinaryTree()
TREEBANK_PATH = '../treebank/treebank.json'
OUTPUT_PATH = '../output_files/jht_trees/'

with open(TREEBANK_PATH) as f:
    data = json.load(f)

# get only songs with trees (150)
JHT_TREES= [song for song in data if song.get('trees')]

def build_tree(d:dict):
    """
    Returns BinaryTree
    """
    if len(d['children']) == 0:
        return BinaryTree(d["label"], EMPTY, EMPTY)
    else:
        return BinaryTree(d["label"],
                          build_tree(d["children"][0]),
                          build_tree(d["children"][1]))


def to_chord_symbol(label:str,corpus="iRb"):
    """
    Parameters:
        * label : string representing a chord name
        * corpus : optionnal parameter usefuL for JHT parsing only
    Returns:
        music21.harmony.ChordSymbol
    """
    return ChordSymbol(replace_chord_symbol(label,corpus))


def intervals(label1:str, label2:str, corpus="iRb")->str:
    """
    Parameters:
        * label1 & label2 : nodes labels of the jht tree
        * corpus : optionnal parameter usefuL for JHT parsing only
    Returns :
         Return this interval as a number 1-7, that is,
         within an octave, but unlike simpleDirected or
         simpleUndirected, turn descending
         seconds into sevenths, etc. Used for calculating step names.
    """
    c1 =to_chord_symbol(label1,corpus).root()
    c2 = to_chord_symbol(label2,corpus).root()
    res = notesToInterval(c1,c2)
    #print(c1, c2, res.diatonic.mod7)
    return res.diatonic.mod7


##################################
# Nodes with the same father
##################################

def brothers_nodes(tree, output="chords", brothers=[])->list[tuple]:
    """
    Parameters:
        * tree a BinaryTree
        * output : "chords" or "intervals"
        * brothers : accumulator
    Returns :
        * list of pair of chords labels brothers in the tree
        * or list of of intervals between pair of chords
        labels brothers in the tree 
    """
    if tree.is_empty():
        return brothers
    
    if not tree.get_left_subtree().is_empty() and not tree.get_right_subtree().is_empty():
        if output == "chords":
            brothers.append((tree.get_left_subtree().get_data(),
                         tree.get_right_subtree().get_data()))
        elif output == "intervals":
            brothers.append(intervals(tree.get_left_subtree().get_data(),
                         tree.get_right_subtree().get_data(),'JHT'))
    
    brothers_nodes(tree.get_left_subtree(), output, brothers)
    brothers_nodes(tree.get_right_subtree(), output, brothers)
    return brothers

#################################################
# Brothers nodes with depth
#################################################

def brothers_nodes_with_depth(tree, output="chords", depth=1, brothers=[]):
    """
    Parameters:
        * tree a BinaryTree
        * output : "chords" or "intervals"
        * brothers : accumulator
    Returns :
        * list of pair of chords labels brothers in the tree with depth
        * or list of of intervals between pair of chords
        labels brothers in the tree with depth
    """
    if tree.is_empty():
        return []
    if not tree.get_left_subtree().is_empty() and \
       not tree.get_right_subtree().is_empty() :
        if output == "chords":
            brothers.append((depth, tree.get_left_subtree().get_data(),
                         tree.get_right_subtree().get_data()))
        if output == "intervals":
            brothers.append((depth, intervals(tree.get_left_subtree().get_data(),
                         tree.get_right_subtree().get_data(),'JHT')))
             
    brothers_nodes_with_depth(tree.get_left_subtree(), output,
                              depth + 1, brothers)
    brothers_nodes_with_depth(tree.get_right_subtree(), output,
                              depth + 1, brothers)
    return brothers    


#################################################
# Nodes that are leafs with the same father 
#################################################

def sisters_leafs(tree, output="chords", sisters=[])->list[tuple]:
    """
    Parameters:
        * tree a BinaryTree
        * output : "chords" or "intervals"
        * sisters : accumulator
    Returns :
        * list of pair of chords labels sisters leafs in the tree
        * or list of of intervals between pair of sisters leafs
        chords labels in the tree 
    """
    if tree.is_empty():
        return sisters
    
    if not tree.get_left_subtree().is_empty() and \
       not tree.get_right_subtree().is_empty() and\
       tree.get_left_subtree().is_leaf() and \
       tree.get_right_subtree().is_leaf():
        if output == "chords":
            sisters.append((tree.get_left_subtree().get_data(),
                         tree.get_right_subtree().get_data()))
        elif output == "intervals":
            sisters.append(intervals(tree.get_left_subtree().get_data(),
                         tree.get_right_subtree().get_data(),'JHT'))
    
    sisters_leafs(tree.get_left_subtree(), output, sisters)
    sisters_leafs(tree.get_right_subtree(), output, sisters)
    return sisters


#################################################
# Leafs that are not sisters
#################################################
def leafs(tree)->list:
    """
    Returns leafs of tree with infix route
    """
    if tree.is_empty():
        return []
    if tree.get_left_subtree().is_empty() and tree.get_right_subtree().is_empty():
        return [tree.get_data()]
    left = leafs(tree.get_left_subtree())
    right = leafs(tree.get_right_subtree())
    return left + right

#################################################
# list of Nodes that are not leafs
#################################################
def nodes_not_leafs(tree)->list:
    """
    Returns nodes that are not leafs
    """
    if tree.is_empty():
        return []
    elif tree.get_left_subtree().is_empty() and \
               tree.get_right_subtree().is_empty():
        return []
    else :
        return [tree.get_data()] + nodes_not_leafs(tree.get_left_subtree()) \
               + nodes_not_leafs(tree.get_right_subtree())

    
if __name__ =='__main__':
    # Example : "Mac The Knife" tree
    mac = [song['trees'][0]['complete_constituent_tree']
                for song in JHT_TREES
               if song['title']=="Mac The Knife"][0]
    tree = build_tree(mac)
    print(tree)
    filename=OUTPUT_PATH+"jht_mac"
    tree.show(filename)