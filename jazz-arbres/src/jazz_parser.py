#!/usr/bin/python3
# -*- coding: utf-8 -*-

__author__ = 'Patrice THIBAUD'
__date_creation__ = 'Fri Jan 13 2023'
__doc__ = """
:mod:`jazz_parser` module
:author: {:s} 
:creation date: {:s}
:last revision: Fri Jan 20 2023

parser for **jazz files of irB corpus
and useful fonctions to replace the name of chords
used for irB and JHT
""".format(__author__, __date_creation__)


import re
from music21.harmony import ChordSymbol

def chords_from_jazz(filename:str)->dict:
    """
    From a jazz file extract metadatas and chords
    Returns dictionnary with keys:values
        * name:str
        * compositors:list[str]
        * lyricist:str
        * year:str
        * bar:str
        * form:str (if given)
        * chords:list[dict] with section for each chord if there's a structure 
    """
    datas = dict()
    datas['chords']=[]
    with open(filename,'r',encoding="utf_8") as file:
        line = file.readline()
        section=None  # if there's a structure
        datas['compositors'] = []
        while line:
            if line.startswith('!!!OTL'):
                datas['name'] = line.split(':')[-1].strip()
            elif line.startswith('!!!COM'):
                datas['compositors'].append(line.split(':')[-1].strip())
            elif line.startswith('!!!LYR'):
                datas['lyricist'] = line.split(':')[-1].strip()
            elif line.startswith('!!!ODT'):
                datas['year'] = line.split(':')[-1].strip()
            elif line.startswith('**jazz'):
                line = file.readline()
                if not line.startswith('*M'):
                    datas['form'] = line.strip('*>')[0] # fonction à réaliser
                    line = file.readline()
                    if line.startswith('*>'):
                        section = line.strip('*>')[0]
                        line = file.readline()
                datas['bar'] = line.split('M')[-1].strip()
                line = file.readline()
                datas['tone'] = split_chord(line)  
            elif line.startswith('*>'):
                section = line.strip('*>')[0]
            elif line[0] not in '*=!\n':
                chord = split_chord(line, datas['bar'])
                if chord is not None: # when occurs r means N.C. nochord
                    datas['chords'].append({'chord':chord,
                                        'section':section})
            line = file.readline()
    return datas


def split_chord(line:str, bar=None):
    """
    From a textual representation of a jazz chord
    build a ChordSymbol
    Parameters:
        * line : str from jazzfile to parse
        * bar : bar measure 
    Returns:
        music21.harmony.ChordSymbol with duration if it's not the tone, without if it is.
    """
    chord_description = line.strip()
    symbol=replace_chord_symbol(chord_description[1:])
    if symbol.startswith('r'): # means NO CHORD
        return None
    elif bar is not None:
        unit=int(bar.split('/')[-1]) # unit of time from bar
        d=int(chord_description[0])*unit
        return ChordSymbol(symbol, duration=d)
    else :
        return ChordSymbol(symbol)
 

def replace_chord_symbol(chord:str, corpus="iRb")->str:
    """
    Returns correct chord symbol for music21
    """
    chord = chord.replace(':', '')
    chord = chord.replace('b9', '')
    chord = chord.replace('maj9', 'maj')
    chord = chord.replace('b13', '')
    chord = chord.replace('#9', '')
    chord = chord.replace('69', '')
    chord = chord.replace('sus', '')
    chord = chord.replace('u', '')
    chord = chord.replace('*7+*', '')
    chord = chord.replace('^', '')
    chord = chord.replace('#11', '')
    chord = chord.replace('h', 'o')
    chord = chord.replace('11', '7')
    chord = chord.replace('alt', '#5')
    if corpus == "JHT":
        chord = chord.replace('%', 'o') # for JHT
        chord = chord.replace('b', '-') # for JHT
    chord = re.sub('/.*', '', chord)
    chord = re.sub('\(.*', '', chord)
    chord = re.sub('\.', '', chord)
    chord=chord.strip(';')
    return chord