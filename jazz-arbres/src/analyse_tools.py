#!/usr/bin/python3
# -*- coding: utf-8 -*-

__author__ = 'Patrice THIBAUD'
__date_creation__ = 'Fri Jan 13 2023'
__doc__ = """
:mod:`analyse_tools` module
:author: {:s} 
:creation date: {:s}
:last revision: 'Tue Jan 31 2023'

tools using music21
for determine intervals between ChordSymbol.root
Transpose ChordSymbol to C
And generate statistics from a list of chords.
""".format(__author__, __date_creation__)


from jazz_parser import *
from music21.interval import Interval

def consecutives_intervals(chords:list[dict])->list[dict]:
    """
    Parameters
        * chords : contains the list of chords produced by
        the jazz_parser. Each chord is represented by a dictionnary
        with keys 'chord" a music21.harmony.ChordSymbol and
        'section' a string represeting the section of the form if given
    Returns:
        list of intervals between two consecutive chords
        each interval is represented by a dictionnary with keys
        chord1, chord2, name
    """
    intervals=[]
    n = len(chords)
    for i in range(n-2):
        interval = Interval(chords[i]['chord'].root(),
                                             chords[i+1]['chord'].root())
        intervals.append({'chord1':chords[i],
                          'chord2':chords[i+1],
                          'name':interval.name})
    return intervals


def intervals_occurences(l:list)->dict:
    """
    From a list of intervals name returns a dictionnary
    of the occurences of each intervals
    """
    res = {}
    for interval in l:
        if res.get(interval) is None:
            res[interval] = 1
        else:
            res[interval] += 1
    return res


def freq_intervals (occ:dict)-> tuple[dict]:
    """
    Returns
        * A dictionnary of the frequencies of each interval
        * A dictionnary of the frequencies of each interval
        without counting P1
    """
    total = 0
    total_without_P1 = 0
    for k, v in occ.items():
        total += v
        if k !="P1":
            total_without_P1 +=v
    res = {}
    res_without_P1 = {}
    for k in occ.keys():
        res[k]=occ[k]*100/total
        res_without_P1[k]=occ[k]*100/total_without_P1 
    return res, res_without_P1

            
def intervals_to_file(intervals, filename:str):
    """
    Create a file named xxx_anayse.txt
    where xxx is parameter filename
    Contains Intervals between roots of two consecutive chords
    """
    l = []
    for interval in intervals:
        l.append(f"{interval['chord1']['chord'].figure:6s} -> \
{interval['chord2']['chord'].figure:6s} : {interval['name']}")
        s = ""
    for e in l:
        s += e+'\n'
    with open(filename+"_intervals.txt","w") as file:
        for line in l:
            file.write(line+"\n")
   
   
def consecutive_intervals_stats(intervals:list[dict], int_name:str)->tuple:
    """
    Parameters:
        * intervals : each interval is represented by a dictionnary with keys
        chord1, chord2, name
        * name : name of the interval being detected
    Returns :
        4-uplet with
            - list of dict(consecutive intervals, frequencies associated)
            - number of selected interval detected
            - number of intervals total
            - name of the interval detected
    """
    stats = {}
    n_total = len(intervals)
    n = 0
    for interval in intervals:
        if interval['name'] == int_name:
            n += 1
            key = (str(interval['chord1']['chord'].root())[0],
                   str(interval['chord2']['chord'].root())[0])
            if key in stats.keys():
                stats[key] += 1
            else:
                stats[key] = 1
    consecutives_chords = []
    for key, value in stats.items():
        consecutives_chords.append({'chords':f'{key[0]}->{key[1]}', 'freq':value})
    consecutives_chords.sort(key=lambda x: x['freq'], reverse=True)
    return consecutives_chords, n, n_total, int_name


def intervals_stats_to_file(consecutive_int, filename):
    """
    Parameters:
        * consecutive : produce by consecutive_intervals_stats function
        * filename (entire corpus or one only file.jazz)
    Creates :
        a file named xxx_stats.txt
        where xxx is parameter filename
        Contains statistics of the presence of an interval given in parameter
        between two consecutive chords
    """
    consecutives_chords, n, n_total, int_name = consecutive_int
    f = n*100/n_total
    with open(filename +"_stats.txt","w") as file:
        file.write(f'{n} occurences of {int_name} that is {f:.1f}% of {n_total} intervals \n')
        file.write(f'occurences of each {int_name} :  \n')
        for c in consecutives_chords:
            file.write(c['chords']+" : "+str(c['freq'])+"\n")
            

def is_interval(c1, c2, interval)->bool:
    """
    Parameters:
        * c1 & c2  : music21.harmony.ChordSymbol
        * interval : music21.interval.Interval.name
    Returns
        * True if the interval between c1 and c2 as the good name
        * False otherwise
    """
    return Interval(c1.root(), c2.root()) == interval


def chords_to_c(chords:list[dict], tone)->list[dict]:
    """
    Parameters:
        * chords : contains the list of chords produced by
        the jazz_parser. Each chord is represented by a dictionnary
        with keys 'chord" a music21.harmony.ChordSymbol and
        'section' a string represeting the section of the form if given
        * tone : a ChordSymbol representing the tone of the piece
    SideEffect:
        Transposed to C all ChordSymbol in chords
    """
    transpose_interval = Interval(tone.root(), ChordSymbol("C").root())
    shift = int(transpose_interval.cents/100)
    for c in chords:
        c['chord'] = c['chord'].transpose(shift)