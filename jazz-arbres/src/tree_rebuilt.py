#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
from treebank_tools import *
from tree_compare_tools import *

TREE_PATH = '../output_files/trees/'

EMPTY = BinaryTree()

OTHER = ['A8','m3','m6','M3','m2','A1',
     'm7','A4','d5','A2','A3','d4','d1']


SEQUENCES = [[['P1*'], ['P1'], ['P4'], ['P1*'], ['P1']],
             [['P1*'], ['P1'], ['P4'], ['P1*'], ['P1'], ['P4'], ['P1*'], ['P1'], ['P5','M2','M6'], ['P1*'], ['P1']],
             [['P1*'], ['P1'], ['P4'], ['M7'], ['P1*'], ['P1'], ['P4'], ['P5', 'M2','M6'], ['P1*'], ['P1'], ['P4'], ['P1*'], ['P1']],
             [['P1*'], ['P1'], ['P4'], ['M7'], ['P1*'], ['P1'], ['P4'], ['P5', 'M2','M6'], OTHER,  ['P1*'], ['P1'], ['P4'], ['P1*'], ['P1']]]


MAX_ITERATION = 1 #max iterations 

#-------------------------------------------------------------
# Algo with trace of execution
#-------------------------------------------------------------

def build_tree_from_leafs_with_trace(tab:list, sequence:list)->list:
    """
    Implementation of our own algorithm
    Parameters:
        * tab: list of JHT leafs (each BinaryTree with EMPTY child)
        * sequence : list of list of intervals
                    (P1* is special : use to detect group of 3 consecutive P1)
    Returns:
        * list of rebuild subtrees
    """
    leafs_trees= [BinaryTree(c, EMPTY, EMPTY) for c in tab]
    iteration = 0
    while(len(leafs_trees)!=1) and iteration < MAX_ITERATION:
            for n in sequence:
                print("n:",n)
                i=0
                while i < len(leafs_trees)-1:
                    print("i = : ", i)
                    print('   ', [x.get_data() for x in leafs_trees])
                    if "P1*" in n: # P1* ---> 3 P1 sequence, group 2 by 2
                        if len(leafs_trees[i:])>=4 and \
                           intervals(leafs_trees[i].get_data(),
                                         leafs_trees[i+1].get_data(),'JHT') == 'P1' and\
                            intervals(leafs_trees[i+1].get_data(),
                                         leafs_trees[i+2].get_data(),'JHT') == 'P1' and\
                            intervals(leafs_trees[i+2].get_data(),
                                         leafs_trees[i+3].get_data(),'JHT') == 'P1':
                            print('P1* détecté')
                            leafs_trees[i]=BinaryTree(leafs_trees[i+1].get_data(),
                                                               leafs_trees[i],
                                                               leafs_trees[i+1])
                            leafs_trees[i+2]=BinaryTree(leafs_trees[i+3].get_data(),
                                                               leafs_trees[i+2],
                                                               leafs_trees[i+3])
                            del leafs_trees[i+1]
                            del leafs_trees[i+2]
                            print('tableau modifié:   ', [x.get_data() for x in leafs_trees])
                            i+=2 
                        else:
                            i+=1
                    elif intervals(leafs_trees[i].get_data(),
                                   leafs_trees[i+1].get_data(),'JHT') in n:
                        leafs_trees[i]=BinaryTree(leafs_trees[i+1].get_data(),
                                                       leafs_trees[i],
                                                       leafs_trees[i+1])
                        print(n, "détecté")
                        del leafs_trees[i+1]
                    else :
                        i+=1
                    iteration +=1
    return leafs_trees

#-------------------------------------------------------------
# Algo
#-------------------------------------------------------------
def build_tree_from_leafs(tab:list, sequence:list)->list:
    """
    Implementation of our own algorithm
    Parameters:
        * tab: list of JHT leafs (each BinaryTree with EMPTY child)
        * sequence : list of list of intervals
                    (P1* is special : use to detect group of 3 consecutive P1)
    Returns:
        * list of rebuild subtrees
    """
    leafs_trees= [BinaryTree(c, EMPTY, EMPTY) for c in tab]
    iteration = 0
    while(len(leafs_trees)!=1) and iteration < MAX_ITERATION:
            for n in sequence:
                i=0
                while i < len(leafs_trees)-1:
                    if "P1*" in n: # P1* ---> 3 P1 sequence, group 2 by 2
                        if len(leafs_trees[i:])>=4 and \
                           intervals(leafs_trees[i].get_data(),
                                         leafs_trees[i+1].get_data(),'JHT') == 'P1' and\
                            intervals(leafs_trees[i+1].get_data(),
                                         leafs_trees[i+2].get_data(),'JHT') == 'P1' and\
                            intervals(leafs_trees[i+2].get_data(),
                                         leafs_trees[i+3].get_data(),'JHT') == 'P1':
                            leafs_trees[i]=BinaryTree(leafs_trees[i+1].get_data(),
                                                               leafs_trees[i],
                                                               leafs_trees[i+1])
                            leafs_trees[i+2]=BinaryTree(leafs_trees[i+3].get_data(),
                                                               leafs_trees[i+2],
                                                               leafs_trees[i+3])
                            del leafs_trees[i+1]
                            del leafs_trees[i+2]
                            i+=2 
                        else:
                            i+=1
                    elif intervals(leafs_trees[i].get_data(),
                                   leafs_trees[i+1].get_data(),'JHT') in n:
                        leafs_trees[i]=BinaryTree(leafs_trees[i+1].get_data(),
                                                       leafs_trees[i],
                                                       leafs_trees[i+1])
                        del leafs_trees[i+1]
                    else :
                        i+=1
                    iteration +=1
    return leafs_trees


if __name__ == '__main__':
    try :
        name= sys.argv[1]
        jht_song = [song['trees'][0]['complete_constituent_tree']
                        for song in JHT_TREES
                       if song['title']==name][0]
    
    except IndexError:
        name = "Interplay"
        jht_song = [song['trees'][0]['complete_constituent_tree']
                        for song in JHT_TREES
                       if song['title']==name][0]
    
    # get leafs from the jht tree
    tree_from_jht = build_tree(jht_song)
   
    # Re-build tree from leafs
    tab_leafs = leafs(tree_from_jht)
    tree_from_algo = build_tree_from_leafs_with_trace(tab_leafs, SEQUENCES[3])
    
    if True:
        # to show jht tree with counter on nodes
        add_leaf_labels(tree_from_jht, 1)
        propagate_labels(tree_from_jht)
        tree_from_jht.show(filename=TREE_PATH+name+'_jht')
        
        # to show rebuilt tree with counter on nodes
        start_label = 1
        for tree in tree_from_algo:
            add_leaf_labels(tree, start_label)
            start_label += count_leafs(tree)
            propagate_labels(tree)
        