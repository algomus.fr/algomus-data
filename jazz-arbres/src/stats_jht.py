#!/usr/bin/python3
# -*- coding: utf-8 -*-

from treebank_tools import *
from analyse_tools import freq_intervals,  intervals_occurences
import sys
import os

OUTPUT_DIRECTORY = "../output_files/stats/"

def write_file(freq, freq_withoutP1, res, s):
    if not os.path.exists(OUTPUT_DIRECTORY):
        os.makedirs(OUTPUT_DIRECTORY)
    with open(OUTPUT_DIRECTORY+'jht_'+s+'_stats.txt','w') as file:
            file.write(f' occurences of each interval in JHT {s} :  \n')
            for k, v in sorted(res.items(), key=lambda x:x[1], reverse = True):
                file.write(f'{k} : {v} \n')
            file.write(f' frequencies of each interval in JHT {s} \
    with and without P1:  \n')
            #for k in freq.keys():
            for k,v in sorted(freq.items(), key=lambda x:x[1], reverse = True):
                file.write(f'{k} : {freq[k]:.1f} % --- {freq_withoutP1[k]:.1f} % \n')


if __name__ == '__main__':
    try:
        # need an argument : "brothers" or "sisters" or "leafs" or "depth"
        nodes = sys.argv[1]
        trees_json = [song['trees'][0]['complete_constituent_tree']
                    for song in JHT_TREES]
        trees = [build_tree(tree) for tree in trees_json]
        
        if nodes != "leafs":
            int_names=[]
            if nodes == "brothers":
                for tree in trees:
                    int_names+=brothers_nodes(tree, output="intervals", brothers=[])
                s = "brothers_nodes"
                
            elif nodes == "sisters":
                for tree in trees:
                    int_names+=sisters_leafs(tree, output="intervals", sisters=[])
                s = "sisters_leafs"
                
            elif nodes == "depth":
                depth4_and_more={}
                for tree in trees:
                    int_names+=brothers_nodes_with_depth(tree, output="intervals", brothers=[])
                s = "brothers_nodes_with_depth"
                # Different analyse if depth (by depth and intervals)
                res= intervals_occurences(int_names)
                depths = set([k[0] for k,v in res.items()])
                for d in depths :
                    res_level = {k:v for k,v in res.items() if k[0]==d}
                    freq, freq_withoutP1 = freq_intervals(res_level)
                    if d>=4:
                        for k in res_level:
                            if not depth4_and_more.get(k[1]):
                                depth4_and_more[k[1]] = res_level[k]
                            else :
                                depth4_and_more[k[1]] += res_level[k]
                    with open(OUTPUT_DIRECTORY+'jht_'+s+'_'+str(d)+'_stats.txt','w') as file:
                        file.write(f' frequencies of each interval in JHT {s}\
 by depth {d}:  \n')
            #for k in freq.keys():
                        for k,v in sorted(freq.items(), key=lambda x:x[1], reverse = True):
                            file.write(f'{k[1]} : {freq[k]:.1f} %  - occurences : {res[k]}\n')
                            
                with open(OUTPUT_DIRECTORY+'jht_depth4_and_more_stats.txt','w') as file:
                    t = sum(depth4_and_more.values())
                    for k in depth4_and_more:
                        file.write(f'{k} : {depth4_and_more[k]*100/t:.2f}\n') 
        

        elif nodes == "leafs":
            all_chords_seq=[]
            for tree in trees:
                all_chords_seq.append(leafs(tree))
            int_names=[]
            for seq in all_chords_seq:
                for i in range(len(seq)-1):
                    int_names.append(intervals(seq[i],seq[i+1],'JHT'))
            s ="leafs"
              
    except IndexError:
        print("brothers, sisters, depth, leafs")
    
    # Equivalent analyse for sisters, leafs et brothers
    if nodes != "depth":
        res= intervals_occurences(int_names)
        freq, freq_withoutP1 = freq_intervals(res)
        write_file(freq, freq_withoutP1, res, s)
            
        