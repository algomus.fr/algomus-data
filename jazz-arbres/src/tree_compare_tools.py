from treebank_tools import nodes_not_leafs

#--------------------------------------------------------
# tools for generate counter labels and propagate them
#--------------------------------------------------------
def add_leaf_labels(tree, start_label):
    """
    Change labels of a tree by adding a counter at the leafs
    The labels became tuple (label, counter)
    """
    if tree.get_left_subtree().is_empty() and tree.get_right_subtree().is_empty():
        tree.set_data((tree.get_data(), start_label))
        return start_label + 1
    else:
        next_label = start_label
        if not tree.get_left_subtree().is_empty():
            next_label = add_leaf_labels(tree.get_left_subtree(), next_label)
        if not tree.get_right_subtree().is_empty():
            next_label = add_leaf_labels(tree.get_right_subtree(), next_label)
        return next_label
    
    
def count_leafs(tree):
    if tree.is_empty():
        return 0
    elif tree.get_left_subtree().is_empty() and tree.get_right_subtree().is_empty():
        return 1
    else :
        return count_leafs(tree.get_left_subtree()) + count_leafs(tree.get_right_subtree())


def propagate_labels(tree):
    """
    Propagate counter labels of the left child nodes
    to the internal nodes of the tree
    
    Parameters:
        * tree with counter on leafs : (chord, counter)
    """
    if not tree.is_empty():
        if not tree.get_left_subtree().is_empty():
            propagate_labels(tree.get_left_subtree())
        if not tree.get_right_subtree().is_empty():
            propagate_labels(tree.get_right_subtree())
            tree.set_data((tree.get_data(), tree.get_right_subtree().get_data()[1])) # remontee compteur droit

#-----------------------------------------------------------
# tools for evaluate the differences between internal nodes
#-----------------------------------------------------------
def tree_to_dict(tree:list)->dict:
    """
    Parameters:
        * tree : each nodes contains label (chord, counter) 
    Returns :
        * dictionnary of occurences of the labels only for the internal nodes
    """
    nodes = nodes_not_leafs(tree)
    res = {}
    for node in nodes:
        if res.get(node) is None:
            res[node] = 1
        else: res[node] += 1
    return res
    
    
def score(d1:dict, dref:dict)->tuple[int,int,int]:
    """
    Parameters:
        * d1 : dictionnary of occurences for the rebuild-tree
        * dref : idem but for the JHT reference tree
    Returns :
        * 3-uplet : N internal nodes well predicted, N bad, total
    """
    total = sum(d1.values()) # total of internal nodes
    good, bad = 0, 0
    for k,v in d1.items():
        if dref.get(k):
            if d1[k] <= dref[k]:
                good += d1[k]
            elif d1[k]> dref[k] :
                good += dref[k]
                bad += d1[k]-dref[k]
        else :
            bad += d1[k]
    assert total == good + bad
    return good, bad, total
            

    
if __name__ == '__main__':
    from binary_tree import *
    
    # example of use
    
    EMPTY = BinaryTree()

    tree1 = BinaryTree('A', EMPTY, EMPTY)
    tree2 = BinaryTree('B', EMPTY, EMPTY)
    tree3 = BinaryTree('C', EMPTY, EMPTY)
    tree4 = BinaryTree('D', EMPTY, EMPTY)
    tree5= BinaryTree('E', tree1, tree2)
    tree6= BinaryTree('F', tree3, tree4)
    tree = BinaryTree('G', tree5, tree6)

    add_leaf_labels(tree, 1)
    propagate_labels(tree)
    tree.show()
