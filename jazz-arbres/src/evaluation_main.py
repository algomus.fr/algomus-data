from tree_rebuilt import *
from jht_list import JHT_TITLES
import os

OUTPUT_EVALUATION = "../output_files/evaluation/"

if __name__ == '__main__':
    if not os.path.exists(OUTPUT_EVALUATION):
        os.makedirs(OUTPUT_EVALUATION) 
         
    total_sensitivity = {i:0 for i in range(len(SEQUENCES))} # sum of sensitivity for all files by sequence
    total_accuracy= {i:0 for i in range(len(SEQUENCES))} # same for accuracy
    sensitivity_up_levels=[{0.9:0, 0.8:0, 0.7:0, 0.3:0} for i in range(len(SEQUENCES))]
    accuracy_up_levels=[{0.9:0, 0.8:0, 0.7:0, 0.3:0} for i in range(len(SEQUENCES))]
    
    for name in JHT_TITLES:
        s="" # string for the file output
        jht_song = [song['trees'][0]['complete_constituent_tree']
                            for song in JHT_TREES
                           if song['title']==name][0]
        
        tree_from_jht = build_tree(jht_song)
        add_leaf_labels(tree_from_jht, 1)
        propagate_labels(tree_from_jht)
        nodes_jht =  tree_to_dict(tree_from_jht)
        N_nodes_jht = sum(nodes_jht.values())
        
        for i in range(len(SEQUENCES)):
            tree_from_jht = build_tree(jht_song)
            tab_leafs = leafs(tree_from_jht)
            tree_from_algo = build_tree_from_leafs(tab_leafs, SEQUENCES[i])
            
            good = 0
            bad = 0
            total = 0
            start_label = 1
            for tree in tree_from_algo:
                add_leaf_labels(tree, start_label)
                start_label += count_leafs(tree) # continuity of numerotation over sub-trees
                propagate_labels(tree)
                nodes_rebuild =  tree_to_dict(tree)
                g,b,t = score(nodes_rebuild, nodes_jht)
                #s += f'{score(nodes_rebuild, nodes_jht)}\n' # to visualize details for all subtrees
                good+=g
                bad+=b
                total+=t
            if total !=0:
                accuracy = good/total
            if total == 0:
                accuracy =0 # only for Afro Blue with sequence P1* P4 (leafs not group)
            sensitivity = good/N_nodes_jht
            s+= f'ok {good} not ok {bad} total {total} JHT {N_nodes_jht} '
            s+= f'precision {accuracy:.2f} sensitivite {sensitivity:.2f} '
            
            # for pretty print
            seq = str(SEQUENCES[i])
            car = "'[]"
            for j in range(len(car)):
                seq = seq.replace(car[j],"")
            s+= f'  {name}  {seq}\n'
            # end
            
            # details for the best modelisation over the 150 songs 
            if accuracy >=0.9:
                accuracy_up_levels[i][0.9] += 1
            if accuracy >=0.8:
                accuracy_up_levels[i][0.8] += 1
            if accuracy >=0.7:
                accuracy_up_levels[i][0.7] += 1
            if accuracy <0.3:
                accuracy_up_levels[i][0.3] += 1
            if sensitivity >=0.9:
                sensitivity_up_levels[i][0.9] += 1
            if sensitivity >=0.8:
                sensitivity_up_levels[i][0.8] += 1
            if sensitivity >=0.7:
                sensitivity_up_levels[i][0.7] += 1
            if sensitivity <0.3:
                sensitivity_up_levels[i][0.3] += 1
            
            total_accuracy[i] += accuracy
            total_sensitivity[i] += sensitivity
            
        name = name.strip('?.') # filenames like 'Got A Match ?'    
        with open(OUTPUT_EVALUATION+name+'_evaluation.txt','w') as file:
            file.write(s)
    
    with open(OUTPUT_EVALUATION+'REBUILD_total_evaluation.txt','w', encoding="utf-8") as file:
        s = ""
        for i in range(len(SEQUENCES)):
            s += f' ----------------------\n sequence : {SEQUENCES[i]} \n'
            s += f'précision moyenne : {total_accuracy[i]/len(JHT_TITLES):.2f} \n'
            s += f'sensitivité moyenne : {total_sensitivity[i]/len(JHT_TITLES):.2f} \n'
            s+= f' nombre de morceaux de précision <0.3: {accuracy_up_levels[i][0.3]} \n'
            s+= f' nombre de morceaux de précision >=0.7 : {accuracy_up_levels[i][0.7]} \n'
            s+= f' nombre de morceaux de précision >=0.8 : {accuracy_up_levels[i][0.8]} \n'
            s+= f' nombre de morceaux de précision >=0.9 : {accuracy_up_levels[i][0.9]} \n'
            s+= f' nombre de morceaux de sensitivité <0.3 : {accuracy_up_levels[i][0.3]} \n'
            s+= f' nombre de morceaux de sensitivité >=0.7 : {accuracy_up_levels[i][0.7]} \n'
            s+= f' nombre de morceaux de sensitivité >=0.8 : {accuracy_up_levels[i][0.8]} \n'
            s+= f' nombre de morceaux de sensitivité >=0.9 : {accuracy_up_levels[i][0.9]} \n'
        file.write(s)