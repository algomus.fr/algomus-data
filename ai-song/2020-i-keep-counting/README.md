
I Keep Counting: An Experiment in Human/AI Co-creative Songwriting

AI Song 2020 contest -- Algomus & Friends -- featuring Niam
<http://www.algomus.fr/i-keep-counting>

We distribute the song, the lead sheet, and raw generations on structures, chords, lyrics, and hook melody.

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit <http://creativecommons.org/licenses/by-sa/4.0/>

