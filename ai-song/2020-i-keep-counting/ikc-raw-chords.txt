# Raw generations of chords

The raw generations of other section has been lost.
Note that we enharmonically rewrote some chords.

## Chorus

Cm A2 F F? Gh7 Fm Em7 G D7 FMaj7 Cm C2 Am7 D# Fm    
C Gm7 A Cm7 C A Cm Bm7 C2 C E2 F#m D# Cm   
GMaj7 EMaj7 A#m F#Maj7 Bh7  
Cm Fm G+2 FMaj7 Fm7 Gm E C D A A? G#m Bm Cm7 Fm7  
G#m F7 Bm7 Gm+2 Am+2 E7 Dnc G#Maj7 Cnc Eh7  
E? D#? Gm7 C A G# F Bm Fm Fm Fm Fm GMaj7 C#+2 A  
G FMaj7 Am F# A? D? F2 Cm7 Fm Cm+2 F#m F? C? Ah7 Gnc  
G2 G#7 D7 Em Cm G Am A# 
Am   
C+2 Cm7 Bbm7 G? Bm Dm7 Fnc AMaj7 A7 F A#2 Fm7 Dm Am Em+2   

## Verse

C C F F? Cnc
F E+2 Gm   
Am+2 F? Eh7 G#m7 A#m7 A#m C Gnc Gnc CMaj7   
?   
C F Am Em Ab C Anc Am Cm C? Dm Dnc    
G Dm7 G7 Cm Dm Bm A F#m   
C D F#Maj7 Gnc   
A Em G G# Eh7 D#m+2 G Gh7 Ah7 Gm G#m F? F D Dh7   
Am F A#7 Em Gm+2 Dm Dm G# Cm  
C Bnc C#m A2 F+2 Dh7 Cm F2 A#m+2 E Fh7 Am F# F#? G7 
